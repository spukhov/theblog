package eightbit.bean;

import eightbit.dao.PostDAO;
import eightbit.dao.UserDAO;
import eightbit.model.Post;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.sql.Date;
import java.util.List;

@Component
@ManagedBean
@RequestScoped
@Scope("session")
public class UserBean {
    public static Logger LOG = Logger.getLogger(UserBean.class);
    private boolean isLogged = false;
    private String login;
    private String password;

    @Autowired
    private UserDAO userDAO;

    public String validateLogin(){
        System.out.println("....... validation");
         if (userDAO.validateLogin(login,password)) {
             isLogged = true;
             System.out.println("VALIDATION = true");
             return "admin";
         }
        FacesMessage msg = new FacesMessage("Login error!", "ERROR MSG");
        msg.setSeverity(FacesMessage.SEVERITY_ERROR);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return "login";
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLogged() {
        return isLogged;
    }

    public void setLogged(boolean logged) {
        isLogged = logged;
    }
}
