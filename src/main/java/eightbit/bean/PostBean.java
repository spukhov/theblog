package eightbit.bean;

import eightbit.dao.PostDAO;
import eightbit.model.Post;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.sql.Date;
import java.util.List;

@Component
@ManagedBean
@RequestScoped
public class PostBean {
    private String id;
    private String topic;
    private String shortText;
    private String content;
    private Date date;
    private List<Post> posts;
    public static Logger LOG = Logger.getLogger(PostBean.class);

    @Autowired
    private PostDAO postDAO;

    public List<Post> getPosts(){
        LOG.info("!!!!!get posts");
        return postDAO.findAll();
    }
}
