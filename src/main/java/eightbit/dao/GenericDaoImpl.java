package eightbit.dao;


import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

public class GenericDaoImpl<T> implements GenericDao<T> {
    @PersistenceContext
    protected EntityManager em;
    private Class< T > type;
    private String name;
    public static Logger LOG = Logger.getLogger(PostDAO.class);

    public GenericDaoImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
        name = type.getSimpleName();
        LOG.info("OLOLOLOOO!!! --- " + name);
    }
    public T create(final T t) {
        em.persist(t);
        return t;
    }

    public void delete(final Object id) {
        em.remove(em.getReference(type, id));
    }
    public T find(final Object id) {
        return (T) em.find(type, id);
    }

    public T update(final T t) {
        return this.em.merge(t);
    }

    public List<T> findAll(){
        LOG.info("find all");
        return em.createQuery("from " + name).getResultList();
    }
}
