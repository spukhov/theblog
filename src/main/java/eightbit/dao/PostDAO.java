package eightbit.dao;

import eightbit.model.Post;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

@Repository
public class PostDAO extends GenericDaoImpl<Post> {
    public static Logger LOG = Logger.getLogger(PostDAO.class);
    public static int counter = 0;

}
