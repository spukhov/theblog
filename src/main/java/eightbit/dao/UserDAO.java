package eightbit.dao;

import eightbit.model.User;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;

@Repository
public class UserDAO extends GenericDaoImpl<User> {
    private String currentUserId;

    public static Logger LOG = Logger.getLogger(UserDAO.class);

    @Override
    public User create(User user){
        String login = user.getLogin();
        if (findUserByLogin(login)==null){
            em.persist(user);
            return user;
        }

        return null;
    }

    public User findUserByLogin(String login) {
        User user = null;
        TypedQuery<User> query = em.createQuery(
                "SELECT u FROM User u WHERE u.login=:login", User.class);
        user = query.setParameter("login", login).getSingleResult();
        return user;
    }

    public boolean validateLogin(String login, String password){
        try{
        User user = findUserByLogin(login);
        LOG.info(login + " - !!! pass:" + user.getPassword());
        if (password.equals(user.getPassword())) return true;
        } catch (Exception e){
            LOG.error(e.getMessage());
        }
        return false;
    }
}
