package eightbit.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Tag")
public class Tag {
    @Id
    @Column(name = "name")
    private String name;

    @ManyToMany (fetch = FetchType.LAZY)
    @JoinTable(name="PostTag", joinColumns=@JoinColumn(name="tagName"),
            inverseJoinColumns=@JoinColumn(name="postId"))
    private List<Post> postList;
}
