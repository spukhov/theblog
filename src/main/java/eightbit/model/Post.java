package eightbit.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table(name = "Post")
public class Post {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "topic")
    private String topic;
    @Column(name = "shortText")
    private String shortText;
    @Column(name = "content")
    private String content;
    @Column(name = "date")
    private Date date;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name="PostTag", joinColumns=@JoinColumn(name="postId"),
            inverseJoinColumns=@JoinColumn(name="tagName"))
    private List<Post> postList;

    public Post(){

    }

    public Post(String topic, String content){
        this.content = content;
        this.topic = topic;
        this.date = (Date)new java.util.Date();
    }

    public Post(String topic, String content, Date date) {
        this.topic = topic;
        this.content = content;
        this.date = date;
    }

    public int getPostId() {
        return id;
    }

    public int setPostId(int postId) {
        return id=postId;
    }

    public String getContent() {
        return content;
    }

    public void setMessage(String content) {
        this.content = content;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }
}