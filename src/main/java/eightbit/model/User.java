package eightbit.model;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "User")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
	private String id;

    @Column(name = "login")
	private String login;
    @Column(name = "password")
	private String password;
    @Column(name = "email")
    private String email;
    @Column(name = "firstname")
	private String firstname;
    @Column(name = "secondname")
	private String secondname;
    @Column(name = "roleId")
    private int roleId;
    @Column(name = "info")
    private String info;

    User(){

    }

    public User(String login, String password, String email, String firstname, String secondname, int roleId, String info) {
        this.login = login;
        this.password = password;
        this.email = email;
        this.firstname = firstname;
        this.secondname = secondname;
        this.roleId = roleId;
        this.info = info;
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}

